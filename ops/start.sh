#!/bin/bash
set -e

root="$(cd "$(dirname "${BASH_SOURCE[0]}")/.." >/dev/null 2>&1 && pwd)"
project="eth"

# make sure a network for this project has been created
docker swarm init 2> /dev/null || true
docker network create --driver overlay "$project" 2> /dev/null || true

if grep -qs "$project" <<<"$(docker stack ls --format '{{.Name}}')"
then echo "An $project stack is already running" && exit 0;
else echo; echo "Preparing to launch $project stack"
fi

########################################
## Load & Log Env Vars

# shellcheck disable=SC1091
if [[ -f ".env" ]]; then source ".env"; fi

ETH_1_CACHE="${ETH_1_CACHE:-2048}"
ETH_1_LIGHT_SERVE="${ETH_1_LIGHT_SERVE:-20}"
ETH_1_URL="${ETH_1_URL:-}"
ETH_2_KEYSTORE="${ETH_2_KEYSTORE:-validator_keys}"
ETH_API_KEY="${ETH_API_KEY:-abc123}"
ETH_DATA_ROOT="${ETH_DATA_ROOT:-$root/.data}"
ETH_DOMAINNAME="${ETH_DOMAINNAME:-}"
ETH_PROD="${ETH_PROD:-false}"

echo "Starting eth stack in env:"
echo "- ETH_1_CACHE=$ETH_1_CACHE"
echo "- ETH_1_LIGHT_SERVE=$ETH_1_LIGHT_SERVE"
echo "- ETH_1_URL=$ETH_1_URL"
echo "- ETH_2_KEYSTORE=$ETH_2_KEYSTORE"
echo "- ETH_API_KEY=$ETH_API_KEY"
echo "- ETH_DATA_ROOT=$ETH_DATA_ROOT"
echo "- ETH_DOMAINNAME=$ETH_DOMAINNAME"
echo "- ETH_PROD=$ETH_PROD"

########################################
# Misc Config

eth1_client="geth"
eth2_client="lighthouse"

if [[ "$ETH_PROD" == "true" ]]
then
  eth1_network="mainnet"
  eth2_network="mainnet"
else
  eth1_network="goerli"
  eth2_network="pyrmont"
fi

mkdir -pv "$ETH_DATA_ROOT/$eth1_client/$eth1_network"
mkdir -pv "$ETH_DATA_ROOT/$eth2_client/$eth2_network"

common="working_dir: '/root'
    networks:
      - '$project'
    logging:
      driver: 'json-file'
      options:
          max-size: '50m'"

########################################
## Setup secrets

password_secret="validator_keystore_password"
if grep -qs "$password_secret\>" <<<"$(docker secret ls)"
then
  echo "A secret called $password_secret already exists, skipping secret setup."
  echo "To overwrite this secret, remove the existing one first: 'docker secret rm $password_secret'"
else
  echo "Enter the $password_secret secret & hit enter (no echo)"
  echo -n "> "
  read -rs secret_value
  echo
  if [[ -z "$secret_value" ]]
  then echo "No secret_value provided, skipping secret creation" && exit 0;
  fi
  if echo "$secret_value" | tr -d '\n\r' | docker secret create "$password_secret" -
  then echo "Successfully saved secret $password_secret"
  else echo "Something went wrong creating a secret called $password_secret" && exit 1
  fi
fi

########################################
## Configure ETH 2 beacon/validator

beacon_internal_port="5025"

eth2_image="${project}_$eth2_client:v$(grep "$eth2_client" versions | awk -F '=' '{print $2}')"

########################################
# Configure Eth1 full node

if [[ -z "$ETH_1_URL" ]]
then
  eth1_http_port="8545"
  eth1_ws_port="8546"
  eth1_image="${project}_$eth1_client:v$(grep "$eth1_client" versions | awk -F '=' '{print $2}')"
  ETH_1_URL="http://eth1:$eth1_http_port"
  echo "Using local ${project}_eth1 service (cache=$ETH_1_CACHE)"
  eth1_service="eth1:
    image: '$eth1_image'
    $common
    environment:
      ETH_1_CACHE: '$ETH_1_CACHE'
      ETH_1_DATADIR: '/root/$eth1_client/$eth1_network'
      ETH_1_HTTP_PORT: '$eth1_http_port'
      ETH_1_LIGHT_SERVE: '$ETH_1_LIGHT_SERVE'
      ETH_1_NETWORK: '$eth1_network'
      ETH_1_WS_PORT: '$eth1_ws_port'
    ports:
      - '30303:30303'
    volumes:
      - '$ETH_DATA_ROOT/$eth1_client:/root/$eth1_client'"

else
  echo "Using remote eth1 provider: $ETH_1_URL"
  eth1_service=""

fi

########################################
# Proxy config

proxy_image="${project}_proxy:v$(grep proxy versions | awk -F '=' '{print $2}')"

if [[ -n "$ETH_DOMAINNAME" ]]
then
  public_url="https://$ETH_DOMAINNAME/$ETH_API_KEY/2"
  proxy_ports="ports:
      - '80:80'
      - '443:443'"
  echo "${project}_proxy will be exposed on *:80 and *:443"

else
  public_port=${public_port:-3000}
  public_url="http://127.0.0.1:$public_port/$ETH_API_KEY/2"
  proxy_ports="ports:
      - '$public_port:80'"
  echo "${project}_proxy will be exposed on *:$public_port"
fi

########################################
## Deploy

if [[ -n "$eth1_image" ]]
then echo "Deploying $eth1_image and $eth2_image"
else echo "Deploying $eth2_image"
fi

docker_compose=.docker-compose.yml
rm -rf $docker_compose
cat -> $docker_compose <<EOF
version: '3.4'

volumes:
  certs:

secrets:
  $password_secret:
    external: true

networks:
  $project:
    external: true

services:

  proxy:
    image: '$proxy_image'
    $common
    $proxy_ports
    environment:
      ETH_1_URL: '$ETH_1_URL'
      ETH_2_HOST: 'beacon:$beacon_internal_port'
      ETH_API_KEY: '$ETH_API_KEY'
      ETH_DOMAINNAME: '$ETH_DOMAINNAME'
    volumes:
      - 'certs:/etc/letsencrypt'

  $eth1_service

  beacon:
    image: '$eth2_image'
    environment:
      ETH_2_DATADIR: '/root/$eth2_client/$eth2_network'
      ETH_1_URL: '$ETH_1_URL'
      ETH_2_INTERNAL_PORT: '$beacon_internal_port'
      ETH_2_MODULE: 'beacon'
      ETH_2_NETWORK: '$eth2_network'
    $common
    volumes:
      - '$ETH_DATA_ROOT/$eth2_client:/root/$eth2_client'

  validator:
    image: '$eth2_image'
    environment:
      ETH_2_BEACON_URL: 'http://beacon:$beacon_internal_port'
      ETH_2_DATADIR: '/root/$eth2_client/$eth2_network'
      ETH_1_URL: '$ETH_1_URL'
      ETH_2_KEYSTORE: '$ETH_2_KEYSTORE'
      ETH_2_MODULE: 'validator'
      ETH_2_NETWORK: '$eth2_network'
      ETH_2_PASSWORD_FILE: '/run/secrets/$password_secret'
    $common
    secrets:
      - '$password_secret'
    volumes:
      - '$ETH_DATA_ROOT/$eth2_client:/root/$eth2_client'
      - '$root/$ETH_2_KEYSTORE:/root/$ETH_2_KEYSTORE'

EOF

docker stack deploy --compose-file "$docker_compose" "$project"

echo "The $project stack has been deployed, waiting for $public_url to start responding.."
timeout=$(( $(date +%s) + 60 ))
while true
do
  res=$(curl -k -m 5 -s "$public_url" || true)
  if [[ -z "$res" || "$res" == *"Waiting for proxy to wake up"* ]]
  then
    if [[ "$(date +%s)" -gt "$timeout" ]]
    then echo "Timed out waiting for $public_url to respond.." && exit
    else sleep 2
    fi
  else echo "Good Morning!"; break;
  fi
done
