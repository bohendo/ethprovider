#!/bin/bash

ETH_2_BEACON_URL="${ETH_2_BEACON_URL:-http://beacon:5052}"
ETH_1_URL="${ETH_1_URL:-http://eth1:8545}"
ETH_2_INTERNAL_PORT="${ETH_2_INTERNAL_PORT:-5025}"
ETH_2_KEYSTORE="${ETH_2_KEYSTORE:-validator_keys}"
ETH_2_MODULE="${ETH_2_MODULE:-beacon}"
ETH_2_NETWORK="${ETH_2_NETWORK:-pyrmont}"
ETH_2_PASSWORD_FILE="${ETH_2_PASSWORD_FILE:-/run/secrets/password}"

ETH_2_DATADIR="${ETH_2_DATADIR:-/data/lighthouse/$ETH_2_NETWORK}"

echo "Starting Lighthouse in env:"
echo "- ETH_2_BEACON_URL=$ETH_2_BEACON_URL"
echo "- ETH_2_DATADIR=$ETH_2_DATADIR"
echo "- ETH_1_URL=$ETH_1_URL"
echo "- ETH_2_INTERNAL_PORT=$ETH_2_INTERNAL_PORT"
echo "- ETH_2_KEYSTORE=$ETH_2_KEYSTORE"
echo "- ETH_2_MODULE=$ETH_2_MODULE"
echo "- ETH_2_NETWORK=$ETH_2_NETWORK"
echo "- ETH_2_PASSWORD_FILE=$ETH_2_PASSWORD_FILE"

function waitfor {
  name=$1
  target=$2
  tmp=${target#*://} # remove protocol
  host=${tmp%%/*} # remove path if present
  if [[ ! "$host" =~ .*:[0-9]{1,5} ]] # no port provided
  then
    echo "$host has no port, trying to add one.."
    if [[ "${target%://*}" == "http" ]]
    then host="$host:80"
    elif [[ "${target%://*}" == "https" ]]
    then host="$host:443"
    else echo "Error: missing port for host $host derived from target $target" && exit 1
    fi
  fi
  echo "Waiting for $name at $target ($host) to wake up..."
  wait-for -t 60 "$host" 2> /dev/null
}

if [[ "$ETH_2_MODULE" == "beacon" ]]
then
  waitfor "eth1" "$ETH_1_URL"
  echo "Running Lighthouse Beacon"
  exec lighthouse --network "$ETH_2_NETWORK" beacon \
    --datadir="$ETH_2_DATADIR" \
    --eth1-endpoints="$ETH_1_URL" \
    --http \
    --http-address="0.0.0.0" \
    --http-allow-origin="*" \
    --http-port="$ETH_2_INTERNAL_PORT"

elif [[ "$ETH_2_MODULE" == "validator" ]]
then

  if [[ ! -f "$ETH_2_PASSWORD_FILE" ]]
  then echo "Password file does not exist at $ETH_2_PASSWORD_FILE, skipping validator import"
  else
    echo "Importing validator accounts from keystore"
    lighthouse --network "$ETH_2_NETWORK" account validator import \
      --datadir="$ETH_2_DATADIR" \
      --directory="$ETH_2_KEYSTORE" \
      --password-file="$ETH_2_PASSWORD_FILE" \
      --reuse-password
  fi
  waitfor "beacon" "$ETH_2_BEACON_URL"
  echo "Running Lighthouse Validator"
  exec lighthouse --network "$ETH_2_NETWORK" validator \
    --datadir="$ETH_2_DATADIR" \
    --beacon-nodes="$ETH_2_BEACON_URL"

else
  echo "Unknown Lighthouse module: $ETH_2_MODULE"
  exit 1

fi
